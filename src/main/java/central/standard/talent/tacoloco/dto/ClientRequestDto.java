package central.standard.talent.tacoloco.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ClientRequestDto {
    private String name;
    private String address;
    private String city;
    private String state;
    private String zip;
    private String telephone;
}
