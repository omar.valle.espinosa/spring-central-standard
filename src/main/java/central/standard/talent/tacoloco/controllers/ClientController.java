package central.standard.talent.tacoloco.controllers;

import central.standard.talent.tacoloco.domain.Client;

import central.standard.talent.tacoloco.dto.ClientRequestDto;
import central.standard.talent.tacoloco.repositories.ClientRepository;
import org.springframework.web.bind.annotation.*;


/**
 * Created by vaeo on 17/11/2021.
 */
@RestController
@RequestMapping("/client")
public class ClientController {

    private final ClientRepository clientRepository;

    public ClientController(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    @PostMapping(value = "/")
    public String createClient(@RequestBody ClientRequestDto request) {
        clientRepository.save(new Client(request.getName(), request.getAddress(), request.getCity(), request.getState(), request.getZip(), request.getTelephone()));
        return "success";
    }

    @GetMapping(value = "/")
    public Iterable<Client> getList() {
        return clientRepository.findAll();
    }
}
