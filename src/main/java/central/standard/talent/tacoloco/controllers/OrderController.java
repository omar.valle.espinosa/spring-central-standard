package central.standard.talent.tacoloco.controllers;

import central.standard.talent.tacoloco.domain.Order;
import central.standard.talent.tacoloco.dto.OrderRequestDto;
import central.standard.talent.tacoloco.repositories.OrderRepository;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by jt on 12/24/19.
 */
@RestController
@RequestMapping("/order")
public class OrderController {

    private final OrderRepository orderRepository;

    public OrderController(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @PostMapping(value = "/")
    public String create(@RequestBody OrderRequestDto orderRequestDto) {
        orderRepository.save(new Order(orderRequestDto.getNumber(), orderRequestDto.getName()));

        return "success";
    }

    @PatchMapping(value = "/")
    public String update(@RequestBody OrderRequestDto orderRequestDto) {
        orderRepository.save(new Order(orderRequestDto.getId(), orderRequestDto.getNumber(), orderRequestDto.getName()));

        return "success";
    }

    @GetMapping(value = "/")
    public Iterable<Order> getList() {
        return orderRepository.findAll();
    }
}
