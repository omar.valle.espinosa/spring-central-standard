package central.standard.talent.tacoloco.bootstrap;

import central.standard.talent.tacoloco.domain.Client;
import central.standard.talent.tacoloco.domain.Order;
import central.standard.talent.tacoloco.domain.Product;
import central.standard.talent.tacoloco.repositories.ClientRepository;
import central.standard.talent.tacoloco.repositories.OrderRepository;
import central.standard.talent.tacoloco.repositories.ProductRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * Created by vaeo on 17/11/2021.
 */
@Component
public class BootStrapData implements CommandLineRunner {

    private final ProductRepository productRepository;
    private final ClientRepository clientRepository;
    private final OrderRepository orderRepository;

    public BootStrapData(ProductRepository productRepository, ClientRepository clientRepository, OrderRepository orderRepository) {
        this.productRepository = productRepository;
        this.clientRepository = clientRepository;
        this.orderRepository = orderRepository;
    }

    @Override
    public void run(String... args) throws Exception {

        System.out.println("Started in Taco Loco");

        Client client = new Client();
        client.setId(new Long(1));
        client.setName("Client 1");
        client.setCity("St Petersburg");
        client.setState("FL");
        client.setAddress("Direccion 1");
        client.setTelephone("5519219026");
        client.setZip("08500");

        clientRepository.save(client);

        System.out.println("Client Count: " + clientRepository.count());

        Product product1 = new Product();
        product1.setName("Producto 1");
        product1.setPrice("1.72");

        Order order1 = new Order();
        order1.setName("Order 1");
        order1.setNumber("Numero de Orden 1");

        clientRepository.save(client);
        orderRepository.save(order1);
        productRepository.save(product1);

        Product product2 = new Product();
        product2.setName("Producto 2");
        product2.setPrice("1.72");
        productRepository.save(product2);

        Order order2 = new Order();
        order2.setName("Order 2");
        order2.setNumber("Numero de Orden 2");
        orderRepository.save(order2);


        System.out.println("Number of Order: " + orderRepository.count());
    }
}
