package central.standard.talent.tacoloco.config;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

  private static final String COMPANY_NAME = "Central Standard Talent";
  private static final String WEB = "https://centralstandardtalent.com/";
  private static final String CONTACT_EMAIL = "omar.valle.espinosa@gmail.com";
  private static final Contact DEFAULT_CONTACT = new Contact(COMPANY_NAME,
      WEB, CONTACT_EMAIL);
  private static final Set<String> DEFAULT_PRODUCES_AND_CONSUMES =
      new HashSet<>(Collections.singletonList("application/json"));

  /**
   * Docket configuration.
   *
   * @return docket.
   */
  @Bean
  public Docket docketConfiguration() {
    return new Docket(DocumentationType.SWAGGER_2).select()
        .apis(RequestHandlerSelectors
            .basePackage("central.standard.talent.tacoloco.controllers"))
        .paths(PathSelectors.regex("/.*"))
        .build()
        .produces(DEFAULT_PRODUCES_AND_CONSUMES)
        .consumes(DEFAULT_PRODUCES_AND_CONSUMES)
        .apiInfo(apiEndPointsInfo());
  }

  private static ApiInfo apiEndPointsInfo() {
    return new ApiInfoBuilder().title("Central Standard Talent")
        .description("Central Standard Talent")
        .contact(DEFAULT_CONTACT)
        .license("Apache 2.0")
        .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")
        .version("1.0.0")
        .build();
  }
}
