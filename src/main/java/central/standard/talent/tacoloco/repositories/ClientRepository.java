package central.standard.talent.tacoloco.repositories;

import central.standard.talent.tacoloco.domain.Client;
import central.standard.talent.tacoloco.domain.Order;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by jt on 12/23/19.
 */
public interface ClientRepository extends CrudRepository<Client, Long> {
}
