package central.standard.talent.tacoloco.repositories;

import central.standard.talent.tacoloco.domain.Product;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by jt on 12/23/19.
 */
public interface ProductRepository extends CrudRepository<Product, Long> {
}
